const i18n = require('i18n');

i18n.configure({
    locales:['en', 'it'],
    directory: __dirname + '/i18n',
    syncFiles: true,
    objectNotation: true,
    register: global
});

global.isMS = process.env.MS == 'true';
module.exports = (settings) => {
    global.settings = settings;

    return {
        middlewares: require('./middlewares'),
        sql: {
            importModels: require('./import-models')
        }
    }
}