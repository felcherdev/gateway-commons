var fs = require('fs'); 
var path = require('path'); 
var admittanceModule = require('admittance'); 
var morgan = require('morgan');
var rfs = require('rotating-file-stream')
var msclient = require('ef-ms-client');

module.exports = {
    lang: (req, res, next, ms = false) => {

        var lang = req.headers['Accept-Language'] || (req.cookies && req.cookies.lang) || settings.defaultLang;

        // Se il client non ha una lingua impostata, entro in modalità di impostazione.
        if (!(req.cookies && req.cookies.lang) || req.headers['Accept-Language']) {
            res.cookie('lang', lang, {
                maxAge: 900000,
                httpOnly: true
            });
        }

        global.lang = lang;
        next();
    },
    logging: (err, req, res, next) => {

        // Middleware dei logs, documentazione: https://github.com/expressjs/morgan .
        var accessLogStream = rfs('access.log', {
            interval: '1d', // rotazione giornaliera.
            path: path.join(__dirname, 'log')
        });

        return morgan('combined', {
            skip: function (req, res) {
                return res.statusCode < 400
            },
            stream: accessLogStream
        })
    },
    RBACSetter: (req, res, next) => {
        var admittance = admittanceModule( global.settings.roles.permissions, global.settings.roles.assignments );
        var rbac = admittance(req.session.user.group);
        
        // Setting a method to easily escape the route in case a user
        // does not have the required permissions.
        req.RBAC = (
            mname,
            ...args
        ) => {
            if(!rbac.is('administrator')){
                var canDoAction = rbac[mname](...args);
                if(!canDoAction) throw new Error(__( "Insufficent permissions to read this resource." ))
            }
        };
        next();
    },
    messages: (err, req, res, next) => {
        // set locals, only providing error in development
        res.locals.message = err.message;
        res.locals.error = req.app.get('env') === 'development' ? err : {};
        // render the error page
        res.status(err.status || 500);

        console.log(err.stack)
        switch(err.name){
            case 'StatusCodeError':
                var body = err.response.body;
                body.origin = "microservices";
                res.status(err.response.statusCode).json(body);
            break;


            case 'SequelizeForeignKeyConstraintError':
                const fk = /FOREIGN KEY \(`(.*)`\) REFERENCES/.exec(err.message);
                res.status(406).json({
                    errors: __('The %s specified has not been found.', fk[1])
                });
            break;
            
            case 'SequelizeEmptyResultError': 
                res.status(406).json({
                    errors: __('Some of the data provided could not be found. Please check that all of the resources specified as URL parameters (if any) do actually exist and try again.')
                });
            break;

            default:
                var resbody = {errors: err.errors || err.message};
                if(req.headers.stack) resbody.stack = err.stack;
                res.json(resbody);
                throw err;
            break;
        }
    },
    authentication: async (req, res, next) => {
        if(!req.session.user) throw new Error(__( "Autenticazione richiesta per effettuare l'accesso a questa sezione." ));
        else {
            var host = process.env.NODE_ENV != 'development' ? req.get('host') : 'dev.easysupp.it';
            if(!req.session.token) req.session.token = await req.ms.authenticate(host);
            next();
        }
    },
    licence: async (req, res, next) => {
        var jwt = req.headers.token;
        var cert = fs.readFileSync(__dirname + '/licence_cert.pem'); // Leggo la chiave pubblica dalla cartella della libreria.
        
        // Verifico il token grazie alla chiave pubblica in modo asimmetrico.
        try {
            jwt.verify(jwt, cert, function (err, decoded) {
                next(decoded);
            });
        } catch(err) {
            res.status(400).json({
                errors: __('Errore durante la verifica del token:') + ' ' + err.message
            })
        }
    },
}