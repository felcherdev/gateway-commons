'use strict';

const appRoot = require('app-root-path');
const fs = require('fs');
const path = require('path');
const basename = path.basename(__filename);
var sequelizeNoUpdateAttributes = require('sequelize-noupdate-attributes');
const namespace = require('cls-hooked').createNamespace('sequelize-transactions');
const Sequelize = require('sequelize');
Sequelize.useCLS(namespace);

const env = process.env.NODE_ENV || 'development';
const config = require(appRoot + '/config/config.js')[env];
const db = {};

// This function allows to import sequelize models inside a directory, with the 
// possibility of extending the sequelize object, and if necessary, the models themselves.

module.exports = (
  modelsDirFromRoot, // sequelize models directory starting from root.
  context, // Usually the request object.
  modelNames
) => {
  let ms = context.ms;
  let sequelize;
  if (config.use_env_variable) {
    sequelize = new Sequelize(process.env[config.use_env_variable], config);
  } else {
    sequelize = new Sequelize(ms.contract.appSlug + '_' + ms.contract.id, config.username, config.password, config);
  }

  // Making microservices and the context available inside the Sequelize model 
  // definition.
  sequelize.ms = ms;
  sequelize.req = context;

  var extend = require(appRoot + modelsDirFromRoot + '/../extend');
  if (extend) extend(sequelize);

  const modelsDir = appRoot + modelsDirFromRoot;
  fs
    .readdirSync(modelsDir)
    .filter(file => {
      return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
    })
    .forEach(file => {
      const model = sequelize['import'](path.join(modelsDir, file));

      model.listMagicMethods = function () {
        for (let assoc of Object.keys(model.associations)) {
          for (let accessor of Object.keys(model.associations[assoc].accessors)) {
            console.log(model.name + '.' + model.associations[assoc].accessors[accessor] + '()');
          }
        }
      }

      db[model.name] = model;

    });

    // Including mailer into sequelize object
    var mailer = require(appRoot + '/mails');
    if(mailer) {
      console.log('Mail templates found, initializing mailer.');
      sequelize.mailer = mailer(sequelize);
    }

  Object.keys(db).forEach(modelName => {
    if (db[modelName].associate) {
      if(!modelNames || modelsNames.include(modelName)) db[modelName].associate(db);
    }
  });


  db.sequelize = sequelize;
  db.Sequelize = Sequelize;
  return db;
};