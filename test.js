const dmms = require('.')({
    appDisplayName: 'EasySupp',
    appName: 'app-easysupp',
    baseNamespace: 'easysupp',
    msHost: 'http://ms01.easysupp.it',         // Microservices host.
    defaultLang: 'it',                                // Default language for app localization.
    hashSalt: 'wHzNZ5DKdcNWAHeRz1HWMT7gSIWxqXCi',
});


(async function(){
    var client = new dmms.DMMSClient();
    await client.authenticate('dev.easysupp.it');

    var users = await client.auth.users.list();
    console.log(users);

})();